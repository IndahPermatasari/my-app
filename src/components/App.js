import React, { Component } from "react";
import { connect } from "react-redux";
import { Icon } from "@material-ui/core";
import { DragDropContext, Droppable } from "react-beautiful-dnd";
import styled from "styled-components";

import List from "./List";
import ActionButton from "./ActionButton";
import { sort, deleteCardDragging } from "../actions";

const AllListContainer = styled.div`
  display: flex;
  flex-wrap: wrap;
`;
const DeleteWrapper = styled.div`
  border-radius 10px;
  border: 4px dashed red;
  color: red;
  display: flex;
  justify-content: center;
  align-items: center;
  width: 250px;
  height 100px;
  position: fixed;
  bottom: 2rem;
  right: 2rem;
`;
class App extends Component {
  onDragEnd = result => {
    const { destination, source, draggableId, type } = result;
    if (!destination) {
      return;
    }
    if (destination.droppableId === "delete") {
      this.props.dispatch(deleteCardDragging(source.droppableId, source.index));
      console.log("deleted");
      return;
    }
    console.log(source);
    console.log(destination);
    this.props.dispatch(
      sort(
        source.droppableId,
        destination.droppableId,
        source.index,
        destination.index,
        draggableId,
        type
      )
    );
  };

  render() {
    const { lists, dispatch } = this.props;
    return (
      <DragDropContext onDragEnd={this.onDragEnd}>
        <div className="App">
          <h2>Trello Remake</h2>
          <Droppable droppableId="all-lists" direction="horizontal" type="list">
            {provided => (
              <AllListContainer
                {...provided.droppableProps}
                ref={provided.innerRef}
              >
                {lists.map((list, index) => (
                  <List
                    key={list.id}
                    listID={list.id}
                    title={list.title}
                    cards={list.cards}
                    index={index}
                    dispatch={dispatch}
                  />
                ))}
                {provided.placeholder}
                <ActionButton list />
              </AllListContainer>
            )}
          </Droppable>
          <Droppable droppableId="delete">
            {provided => (
              <DeleteWrapper
                {...provided.droppableProps}
                ref={provided.innerRef}
              >
                <Icon fontSize="large">delete</Icon>
              </DeleteWrapper>
            )}
          </Droppable>
        </div>
      </DragDropContext>
    );
  }
}

const mapStateToProps = state => ({
  lists: state.lists
});

export default connect(mapStateToProps)(App);
