import React, { Component } from "react";
import { Card, IconButton, Icon, Button } from "@material-ui/core";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import { Draggable } from "react-beautiful-dnd";
import styled from "styled-components";
import TextareaAutosize from "react-textarea-autosize";
import { connect } from "react-redux";

import { updateCard } from "./../actions";

const CardContainer = styled.div`
  margin-bottom: 8px;
`;
const IconButtonStyle = styled(IconButton)`
  visibility: hidden;
  ${CardContainer}:hover & {
    visibility: visible;
  }
`;

class ListCard extends Component {
  state = {
    formOpen: false,
    text: ""
  };
  toggleForm = () => {
    const { text } = this.props;
    this.setState(prevState => ({
      formOpen: !prevState.formOpen,
      text
    }));
  };
  handleInputChange = e => {
    this.setState({ text: e.target.value });
  };
  handleUpdateCard = () => {
    const { dispatch, id, text, listID } = this.props;
    if (this.state.text && text !== this.state.text) {
      dispatch(updateCard(listID, id, this.state.text));
      this.setState({ text: "" });
    }
    return;
  };
  handleFocus = e => {
    e.target.select();
  };
  renderForm = () => {
    const { text } = this.props;

    return (
      <div>
        <Card
          style={{
            minHeight: 80,
            minWidth: 272,
            padding: "6px 8px 2px",
            margin: "6px 0"
          }}
        >
          <TextareaAutosize
            placeholder="Add the ext"
            autoFocus
            onFocus={this.handleFocus}
            onBlur={this.toggleForm}
            value={this.state.text}
            onChange={this.handleInputChange}
            style={{
              resize: "none",
              width: "100%",
              outline: "none",
              border: "none",
              overflow: "visible"
            }}
          />
        </Card>
        <Button
          onMouseDown={this.handleUpdateCard}
          variant="contained"
          style={{
            color: "white",
            backgroundColor: "#5aac44",
            marginBottom: "8px"
          }}
        >
          Save
        </Button>
      </div>
    );
  };
  renderCard = () => {
    const { text, id, index } = this.props;
    return (
      <Draggable draggableId={String(id)} index={{ index, id }}>
        {provided => (
          <CardContainer
            className="card-container"
            ref={provided.innerRef}
            {...provided.draggableProps}
            {...provided.dragHandleProps}
          >
            <Card>
              <CardContent
                style={{
                  display: "flex",
                  justifyContent: "space-between",
                  alignItems: "flex-start"
                }}
              >
                <Typography gutterBottom>{text}</Typography>
                <IconButtonStyle size="small" onClick={this.toggleForm}>
                  <Icon fontSize="small">edit</Icon>
                </IconButtonStyle>
              </CardContent>
            </Card>
          </CardContainer>
        )}
      </Draggable>
    );
  };
  render() {
    console.log("rnned");
    return this.state.formOpen ? this.renderForm() : this.renderCard();
  }
}

export default connect()(ListCard);
