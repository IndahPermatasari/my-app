import React, { Component } from "react";
import Icon from "@material-ui/core/Icon";
import { Card, Button } from "@material-ui/core";
import TextareaAutosize from "react-textarea-autosize";
import { connect } from "react-redux";
import { addList, addCard } from "./../actions";

class ActionButton extends Component {
  state = {
    formOpen: false,
    text: ""
  };
  toggleForm = () => {
    this.setState(prevState => ({ formOpen: !prevState.formOpen }));
  };
  handleInputChange = e => {
    this.setState({ text: e.target.value });
  };
  handleAddList = () => {
    const { dispatch } = this.props;
    const { text } = this.state;
    if (text) {
      dispatch(addList(text));
      this.setState({ text: "" });
    }
    return;
  };
  handleAddCard = () => {
    const { dispatch, listID } = this.props;
    const { text } = this.state;
    if (text) {
      dispatch(addCard(listID, text));
      this.setState({ text: "" });
    }
    return;
  };
  renderAddButton = () => {
    const { list } = this.props;
    const buttonText = list ? "Add another list" : "Add another card";
    const buttonStyles = {
      listStyle: {
        ...styles.buttonGroup,
        opacity: 1,
        color: "white",
        backgroundColor: "rgba(0,0,0,.15)"
      },
      cardStyle: {
        ...styles.buttonGroup,
        opacity: 0.5,
        color: "inherit",
        backgroundColor: "inherit"
      }
    };
    return (
      <div
        onClick={this.toggleForm}
        style={list ? buttonStyles.listStyle : buttonStyles.cardStyle}
      >
        <Icon>add</Icon>
        <p>{buttonText}</p>
      </div>
    );
  };

  renderForm = () => {
    const { list } = this.props;
    const placeholder = list
      ? "Enter list title..."
      : "Enter title for this card";
    const buttonTitle = list ? "Add list" : "Add card";
    return (
      <div>
        <Card
          style={{
            minHeight: 80,
            minWidth: 272,
            padding: "6px 8px 2px"
          }}
        >
          <TextareaAutosize
            placeholder={placeholder}
            autoFocus
            onBlur={this.toggleForm}
            value={this.state.text}
            onChange={this.handleInputChange}
            style={{
              resize: "none",
              width: "100%",
              outline: "none",
              border: "none",
              overflow: "visible"
            }}
          />
        </Card>
        <div style={styles.formButtonGroup}>
          <Button
            onMouseDown={list ? this.handleAddList : this.handleAddCard}
            variant="contained"
            style={{ color: "white", backgroundColor: "#5aac44" }}
          >
            {buttonTitle}
          </Button>
          <Icon style={{ marginLeft: 8, cursor: "pointer" }}>close</Icon>
        </div>
      </div>
    );
  };
  render() {
    return this.state.formOpen ? this.renderForm() : this.renderAddButton();
  }
}
const styles = {
  buttonGroup: {
    display: "flex",
    alignItems: "center",
    cursor: "pointer",
    borderRadius: 3,
    height: 36,
    width: 272,
    paddingLeft: 10
  },
  formButtonGroup: {
    marginTop: 8,
    display: "flex",
    alignItems: "center"
  }
};

export default connect()(ActionButton);
