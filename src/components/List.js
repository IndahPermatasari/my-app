import React, { Component } from "react";
import { Droppable, Draggable } from "react-beautiful-dnd";
import { TextField } from "@material-ui/core";
import styled from "styled-components";

import ListCard from "./ListCard";
import ActionButton from "./ActionButton";
import { updateListTitle } from "./../actions";
const ListContainer = styled.div`
  background-color: #dfe3e6;
  width: 300px;
  padding: 8px;
  margin: 4px;
  height: 100%;
`;
const CustomizedTextField = styled(TextField)`
  width: 100%;
  margin: 10px 0 !important;
  & input {
    padding: 12px;
    font-size: 0.85em;
  }
`;

class List extends Component {
  state = {
    formOpen: false,
    title: ""
  };
  toggleForm = () => {
    const { title } = this.props;
    this.setState(prevState => ({
      formOpen: !prevState.formOpen,
      title
    }));
  };
  handleChangeTitle = e => {
    this.setState({ title: e.target.value });
  };
  handleUpdateListTitle = () => {
    const { title: titleState } = this.state;
    const { title: titleProps, dispatch, listID } = this.props;
    this.toggleForm();
    if (titleState && titleState !== titleProps) {
      console.log("runned");
      dispatch(updateListTitle(listID, titleState));
      this.setState({ title: "" });
      console.log(this.props);
    }
    return;
  };
  handleFocus = e => {
    e.target.select();
  };
  renderForm = () => {
    const { title } = this.state;
    return (
      <CustomizedTextField
        id="outlined-basic"
        autoFocus
        onFocus={this.handleFocus}
        label="Title"
        variant="outlined"
        onBlur={this.handleUpdateListTitle}
        value={title}
        onChange={this.handleChangeTitle}
      />
    );
  };
  render() {
    const { title, cards, listID, index } = this.props;
    const { formOpen } = this.state;
    return (
      <Draggable draggableId={String(listID)} index={index}>
        {provided => (
          <ListContainer
            className="list-container"
            ref={provided.innerRef}
            {...provided.draggableProps}
            {...provided.dragHandleProps}
          >
            <Droppable droppableId={String(listID)}>
              {provided => (
                <div {...provided.droppableProps} ref={provided.innerRef}>
                  {formOpen ? (
                    this.renderForm()
                  ) : (
                    <h4 onClick={this.toggleForm} style={{ marginLeft: 8 }}>
                      {title}
                    </h4>
                  )}
                  {cards.map((card, index) => (
                    <ListCard
                      key={card.id}
                      index={{ card, index }}
                      listID={listID}
                      text={card.text}
                      id={card.id}
                    />
                  ))}
                  {provided.placeholder}
                  <ActionButton listID={listID} />
                </div>
              )}
            </Droppable>
          </ListContainer>
        )}
      </Draggable>
    );
  }
}

export default List;
