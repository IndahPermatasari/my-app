export const addCard = (listID, text) => {
  return {
    type: "ADD_CARD",
    payload: {
      text,
      listID
    }
  };
};

export const updateCard = (listId, cardId, text) => {
  return {
    type: "UPDATE_CARD",
    payload: {
      listId,
      cardId,
      text
    }
  };
};

export const deleteCardDragging = (listIDStart, cardIndex) => {
  return {
    type: "DELETE_CARD_DRAGGING",
    payload: {
      listIDStart,
      cardIndex
    }
  };
};
