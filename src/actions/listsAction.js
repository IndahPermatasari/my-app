export const addList = title => {
  return {
    type: "ADD_LIST",
    payload: title
  };
};
export const updateListTitle = (listId, title) => {
  return {
    type: "UPDATE_LIST_TITLE",
    payload: {
      listId,
      title
    }
  };
};
export const sort = (
  droppableIdStart,
  droppableIdEnd,
  droppableIndexStart,
  droppableIndexEnd,
  draggableId,
  type
) => {
  return {
    type: "DRAGGING",
    payload: {
      droppableIdStart,
      droppableIdEnd,
      droppableIndexStart,
      droppableIndexEnd,
      draggableId,
      type
    }
  };
};
