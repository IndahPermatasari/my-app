import { CONSTANTS } from "./../actions";
import _ from "lodash";

let listID = 3;
let cardID = 6;

const initialState = [
  {
    id: `list-0`,
    title: "To Do",
    cards: [
      {
        id: `card-0`,
        text:
          "Do dishes Do dishes Do dishes Do dishes Do dishes Do dishes Do dishes Do dishes"
      },
      {
        id: `card-1`,
        text: "Do homework"
      }
    ]
  },
  {
    id: `list-1`,
    title: "In Progress",
    cards: [
      {
        id: `card-2`,
        text: "Learning React"
      }
    ]
  },
  {
    id: `list-2`,
    title: "Done",
    cards: [
      {
        id: `card-3`,
        text: "Learning HTML"
      },
      {
        id: `card-4`,
        text: "Learning CSS"
      },
      {
        id: `card-5`,
        text: "Learning JS"
      }
    ]
  }
];

const listsReducer = (state = initialState, action) => {
  switch (action.type) {
    case CONSTANTS.ADD_LIST:
      const newList = {
        title: action.payload,
        cards: [],
        id: `list-${listID}`
      };
      listID += 1;
      const addListState = [...state, newList];
      console.log(addListState);
      return addListState;

    case CONSTANTS.ADD_CARD:
      const newCard = {
        text: action.payload.text,
        id: `card-${cardID}`
      };
      cardID += 1;
      const addCardState = state.map(list => {
        if (list.id === action.payload.listID) {
          return {
            ...list,
            cards: [...list.cards, newCard]
          };
        } else {
          return list;
        }
      });
      console.log(addCardState);
      return addCardState;

    case CONSTANTS.DRAGGING:
      const {
        droppableIdStart,
        droppableIdEnd,
        droppableIndexStart,
        droppableIndexEnd,
        draggableId,
        type
      } = action.payload;
      const newDragState = [...state];
      if (type === "list") {
        const list = newDragState.splice(droppableIndexStart, 1);
        newDragState.splice(droppableIndexEnd, 0, ...list);
        console.log(newDragState);
        return newDragState;
      }
      const { index: indexStart } = droppableIndexStart;
      const { index: indexEnd } = droppableIndexEnd;
      if (droppableIdStart === droppableIdEnd) {
        const list = newDragState.find(list => droppableIdStart === list.id);
        const card = list.cards.splice(indexStart.index, 1);
        list.cards.splice(indexEnd.index, 0, ...card);
      } else {
        const listStart = newDragState.find(
          list => droppableIdStart === list.id
        );
        const card = listStart.cards.splice(indexStart.index, 1);
        const listEnd = newDragState.find(list => droppableIdEnd === list.id);
        if (indexEnd) {
          listEnd.cards.splice(indexEnd.index, 0, ...card);
        } else {
          listEnd.cards.push(...card);
        }
      }
      console.log(newDragState);
      return newDragState;

    case CONSTANTS.DELETE_DRAGGING:
      const deleteDragState = [...state];
      const { listIDStart, cardIndex } = action.payload;
      const targetList = deleteDragState.find(list => listIDStart === list.id);
      _.remove(targetList.cards, card => card.id === cardIndex.id);
      return deleteDragState;

    case CONSTANTS.UPDATE_CARD:
      const updateCardState = [...state];
      const { listId, cardId, text } = action.payload;
      const updateTargetList = updateCardState.find(list => list.id === listId);
      const updateTargetCard = updateTargetList.cards.find(
        card => card.id === cardId
      );
      updateTargetCard.text = text;
      console.log(updateCardState);
      return updateCardState;

    case CONSTANTS.UPDATE_LIST_TITLE:
      const updateListTitleState = [...state];
      const { listId: listIdUpdate, title } = action.payload;
      const updateListTitleList = updateListTitleState.find(
        list => list.id === listIdUpdate
      );
      updateListTitleList.title = title;
      console.log(updateListTitleState);
      return updateListTitleState;
    default:
      console.log(state);
      return state;
  }
};

export default listsReducer;
